#!/bin/bash -l

# These will be provided to the target
FILE=version.txt
VERSION="$(cat "${FILE}")"

docker -l fatal image build --compress --tag "registry.gitlab.com/sense.ai.tion-public/analysis-docker-container/sense-analysis:v${VERSION}" .
docker -l fatal login registry.gitlab.com
docker -l fatal image push "registry.gitlab.com/sense.ai.tion-public/analysis-docker-container/sense-analysis:v${VERSION}"

oldversion=$(cat version.txt)
newversion=$(expr $oldversion + 1)
echo $newversion > version.txt