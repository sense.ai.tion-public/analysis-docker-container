# sense.ai.tion analysis - Analysis CI/CD and Runtime Docker Image
# -------------------------------------
# 
# (c) 2019 by sense.ai.tion GmbH, <Roman.Kobosil@senseaition.com>
# 

# definition of container
FROM golang:latest
LABEL author="sense.ai.tion"
LABEL maintainer="<Roman.Kobosil@senseaition.com>"
LABEL version="1.0"
LABEL description="A Docker image which provides an environment for the execution of all modules in analysis group."


# Replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

# only necessary for running container within docker (kubernetes manages its own environment and configuration)
ENV GOARCH amd64
ENV GOOS linux

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH

RUN mkdir -p "$GOPATH/src" "$GOPATH/bin" && chmod -R 777 "$GOPATH"

RUN curl -sL https://sentry.io/get-cli/ | bash

# installation of necessary dep
RUN curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
RUN go get -u golang.org/x/lint/golint

#install python 3
RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 install --upgrade pip

WORKDIR "$GOPATH/src"

# ENTRYPOINT ./src
